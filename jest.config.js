module.exports = {
    preset: '@vue/cli-plugin-unit-jest',

    clearMocks: true,
    collectCoverage: true,
    collectCoverageFrom: [
        "resources/**/*.(js|vue)"
    ],

    coverageDirectory: "coverage",
    coveragePathIgnorePatterns: [,
        "/node_modules/",
        "resources/components/index.js",
        "resources/plugins/index.js",
        "resources/init.js"
    ],

}